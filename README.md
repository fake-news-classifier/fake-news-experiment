# Fake news classifiers

## Configure Python environment
Create python virtual environment
```shell
### on Windows ###
python -m venv venv
### on Linux/macOS ###
python3 -m venv venv
```
Activate the virtual environment
```shell
### on Windows ###
.\venv\Scripts\activate.bat
### on Linux/macOS ###
source ./venv/bin/activate
```

Install all dependencies in requirements.txt
```shell
pip install -r requirements.txt
```

## Working with american datasets
Generate ```kaggle.json``` with Kaggle API credentials and copy it inside your home directory
```shell
### on Windows ###
copy kaggle.json C:\Users\marco\.kaggle
### on Linux/macOS ###
cp kaggle.json ~/.kaggle
```

Download all the datasets from Kaggle API and clean it
```shell
python american_kaggle_downloader_and_cleaner.py
```

Launch all american_tests
```shell
papermill american_tests/test1.ipynb american_tests/test1.ipynb --cwd american_tests
papermill american_tests/test2.ipynb american_tests/test2.ipynb --cwd american_tests
papermill american_tests/test4.ipynb american_tests/test4.ipynb --cwd american_tests
papermill american_tests/test5.ipynb american_tests/test5.ipynb --cwd american_tests
papermill american_tests/test6.ipynb american_tests/test6.ipynb --cwd american_tests
papermill american_tests/test7.ipynb american_tests/test7.ipynb --cwd american_tests
papermill american_tests/test8.ipynb american_tests/test8.ipynb --cwd american_tests
```

Launch final test
```shell
papermill american_experiment.ipynb american_experiment.ipynb
```

## Working with italian datasets
Crawl the datasets from Wordpress API and sitemap parsing
```shell
python italian_crawler.py
```

Launch final test
```shell
papermill italian_experiment.ipynb italian_experiment.ipynb
```

## Reference

### American Dataset
- Dataset1 (FAKE and REAL): https://www.kaggle.com/clmentbisaillon/fake-and-real-news-dataset
- Dataset2 (FAKE and REAL): https://www.kaggle.com/hassanamin/textdb3
- Dataset3 (FAKE): https://www.kaggle.com/mrisdal/fake-news
- Dataset4 (FAKE and REAL): https://www.kaggle.com/ruchi798/source-based-news-classification
- Dataset5 (FAKE and REAL): https://www.kaggle.com/jruvika/fake-news-detection
- Dataset6 (FAKE and REAL): https://www.kaggle.com/mohamadalhasan/a-fake-news-dataset-around-the-syrian-war
- Dataset7 (FAKE and REAL): https://www.kaggle.com/saratchendra/fake-news
- Dataset8 (FAKE and REAL): https://www.kaggle.com/arashnic/covid19-fake-news