import os

import kaggle
import papermill as pm

DATASETS = (
    (1, 'clmentbisaillon/fake-and-real-news-dataset'),
    (2, 'hassanamin/textdb3'),
    (3, 'mrisdal/fake-news'),
    (4, 'ruchi798/source-based-news-classification'),
    (5, 'jruvika/fake-news-detection'),
    (6, 'mohamadalhasan/a-fake-news-dataset-around-the-syrian-war'),
    (7, 'saratchendra/fake-news'),
    (8, 'arashnic/covid19-fake-news')
)


def download_dataset_with_kaggle_api(dataset_index, dataset_name):
    if not os.path.exists(f'american_datasets/{dataset_index}'):
        os.mkdir(f'american_datasets/{dataset_index}')

    kaggle.api.authenticate()
    kaggle.api.dataset_download_files(dataset=dataset_name, path=f'american_datasets/{dataset_index}', unzip=True)


def clean_dataset(dataset_index, dataset_name):
    print("Cleaning:", dataset_index, dataset_name)
    pm.execute_notebook(
        f'american_cleaners/cleaner{dataset_index}.ipynb',
        f'american_cleaners/cleaner{dataset_index}.ipynb',
        cwd=os.path.join('american_cleaners')
    )


if __name__ == "__main__":
    # Create folder american_datasets
    if not os.path.exists('american_datasets'):
        os.mkdir('american_datasets')

    # Download all datasets in parallel
    for index, name in DATASETS:
        download_dataset_with_kaggle_api(dataset_index=index, dataset_name=name)

    # Create folder american_datasets/cleaned
    if not os.path.exists('american_datasets/cleaned'):
        os.mkdir('american_datasets/cleaned')

    # Execute all cleaner
    for index, name in DATASETS:
        clean_dataset(dataset_index=index, dataset_name=name)
