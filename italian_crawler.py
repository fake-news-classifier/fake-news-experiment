import os
import time

import requests
import pandas as pd
import re
import html
from concurrent.futures import ThreadPoolExecutor

from bs4 import BeautifulSoup
from goose3 import Goose

N_THREAD = 16
OUTPUT_FOLDER = 'italian_dataset'

WP_PAGE_SIZE = 100
WP_MAX_ARTICLES = 5000
WP_API_PATH = "/wp-json/wp/v2/posts"
WP_SITES = [
    ("notiziedalmondooggi", "http://notiziedalmondooggi.altervista.org", "Fake"),
    ("totalitarismo-blog", "https://www.totalitarismo.blog", "Fake"),
    ("movimento3v", "https://www.movimento3v.it", "Fake"),
    ("maurizioblondet", "https://www.maurizioblondet.it", "Fake"),
    ("lacrunadellago", "https://lacrunadellago.net", "Fake"),
    ("databaseitalia", "https://databaseitalia.it", "Fake"),
    ("grandeinganno", "https://grandeinganno.it", "Fake"),
    ("ilfattoquotidiano", "https://www.ilfattoquotidiano.it", "True"),
    ("quotidianomolise", "https://quotidianomolise.com", "True"),
]

SITEMAP_NAME = "sitemap.xml"
SITEMAP_MAX_ARTICLES = 1000
SITEMAPS = {
    ("terrarealtime", "https://terrarealtime.blogspot.com", "Fake"),
    ("ilsapereepotere2", "https://ilsapereepotere2.blogspot.com", "Fake"),
    ("thedayafter2012", "https://thedayafter2012.blogspot.com", "Fake"),
    ("video-reporter", "https://video-reporter.blogspot.com", "Fake"),
    ("ilgiornaledellasera", "https://ilgiornaledellasera.blogspot.com", "Fake"),
    ("verita-globale", "https://verita-globale.blogspot.com", "Fake"),
    ("reietto", "https://ilreietto.com", "Fake"),
    ("6viola", "https://6viola.wordpress.com", "Fake"),
    ("usciredallorrore", "https://usciredallorrore.wordpress.com", "Fake"),
    ("ilfastidioso", "https://ilfastidioso.myblog.it", "Fake"),
    ("uneuropadiversa", "https://uneuropadiversa.it", "Fake"),
}


def html_clean(text):
    text = re.sub(r'(\r\n|\n|\r|\t)', ' ', text)
    text = re.sub(r'https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)', ' ', text)
    text = re.sub(r'<.*?>', ' ', text)
    text = html.unescape(text)
    text = " ".join(text.split())
    return text


def parse_sitemap(main_sitemap):
    all_urls = []
    sitemaps_stack = [main_sitemap]
    while len(sitemaps_stack) > 0:
        current_sitemap = sitemaps_stack.pop()

        # Fetch sitemap body
        sitemap_response = requests.get(current_sitemap)
        soup = BeautifulSoup(sitemap_response.text, 'html.parser')

        # Get all sub-sitemaps elements
        sitemaps = [e.text for e in soup.select('sitemap loc')]
        for sitemap in sitemaps:
            sitemaps_stack.append(sitemap)

        # Get all url elements
        urls = [e.text for e in soup.select('url loc')]
        for url in urls:
            all_urls.append(url)

        # Sleep
        time.sleep(0.3)
    return all_urls


def crawl_wordpress(site_name, url, label):
    df = pd.DataFrame()
    page = 1
    while True:
        print(f"parsing {site_name} - page {page}")
        response = requests.get(url + WP_API_PATH + f"?per_page={WP_PAGE_SIZE}&order_by=date&page={page}&status=publish")
        response = response.json()

        for post in response:
            link = post['link']
            title = html_clean(post['title']['rendered'])
            text = html_clean(post['content']['rendered'])

            df = df.append({"link": link, "title": title, "text": text, 'label': label}, ignore_index=True)
        page += 1
        time.sleep(0.3)

        df.to_csv(f"{OUTPUT_FOLDER}/{label}/{site_name}.csv")
        if len(response) < WP_PAGE_SIZE or df.shape[0] > WP_MAX_ARTICLES:
            break


def crawl_sitemap(site_name, url, label):
    df = pd.DataFrame()

    article_urls = parse_sitemap(f"{url}/{SITEMAP_NAME}")
    print(site_name, len(article_urls))

    for article_url in article_urls:
        article_response = requests.get(article_url)
        title = Goose().extract(raw_html=article_response.text).title
        text = html_clean(Goose().extract(raw_html=article_response.text).cleaned_text)
        df = df.append({"link": article_url, "title": title, "text": text, 'label': label}, ignore_index=True)
        time.sleep(0.3)

        if df.shape[0] > SITEMAP_MAX_ARTICLES:
            break

    df.to_csv(f"{OUTPUT_FOLDER}/{label}/{site_name}.csv")


if __name__ == '__main__':
    # Create output folder
    if not os.path.exists(f'{OUTPUT_FOLDER}'):
        os.mkdir(f'{OUTPUT_FOLDER}')

    # Create folder True
    if not os.path.exists(f'{OUTPUT_FOLDER}/True'):
        os.mkdir(f'{OUTPUT_FOLDER}/True')

    # Create folder Fake
    if not os.path.exists(f'{OUTPUT_FOLDER}/Fake'):
        os.mkdir(f'{OUTPUT_FOLDER}/Fake')

    start_ts = time.time()
    with ThreadPoolExecutor(N_THREAD) as p:
        p.map(lambda args: crawl_wordpress(*args), WP_SITES)
    end_ts = time.time()
    print("Wordpress crawling took", end_ts - start_ts, "s")

    start_ts = time.time()
    with ThreadPoolExecutor(N_THREAD) as p:
        p.map(lambda args: crawl_sitemap(*args), SITEMAPS)
    end_ts = time.time()
    print("Sitemaps crawling took", end_ts - start_ts, "s")
